/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2012  Vishesh Handa <handa.vish@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "nuao.h"
#include "ndo.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QObject>

#include <KAboutData>
#include <KApplication>
#include <KCmdLineArgs>
#include <KCmdLineOptions>
#include <KUrl>
#include <KDebug>

#include <QtCore/QTimer>
#include <QtCore/QDateTime>

#include <nepomuk2/simpleresource.h>
#include <nepomuk2/simpleresourcegraph.h>
#include <nepomuk2/storeresourcesjob.h>
#include <nepomuk2/datamanagement.h>

#include <Nepomuk2/Vocabulary/NFO>
#include <Nepomuk2/Vocabulary/NIE>

using namespace Nepomuk2::Vocabulary;

class App : public QCoreApplication {
    Q_OBJECT
public:
    App(int& argc, char** argv, int flags = ApplicationFlags);

public slots:
    void onJobFinish(KJob* job);

private:
    QUrl m_fileUrl;
    QUrl m_downloadUrl;
    QUrl m_websiteUrl;
};

App::App(int& argc, char** argv, int flags): QCoreApplication(argc, argv, flags)
{
    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
    if( args->count() == 2 ) {
        m_fileUrl = args->url(0);
        m_downloadUrl = args->arg(1);
        m_websiteUrl = m_downloadUrl;
        m_websiteUrl.setPath( KUrl(m_downloadUrl).directory() );

        kDebug() << "File Url: " <<  m_fileUrl;
        kDebug() << "Download Url: " << m_downloadUrl;
        kDebug() << "Website url: " << m_websiteUrl;
    }

    Nepomuk2::SimpleResource remoteFile;
    remoteFile.addType( NFO::RemoteDataObject() );
    remoteFile.addType( NFO::WebDataObject() );
    remoteFile.addProperty( NIE::url(), m_downloadUrl );

    Nepomuk2::SimpleResource file;
    file.addType( NFO::FileDataObject() );
    file.addProperty( NIE::url(), m_fileUrl );
    file.addProperty( NDO::copiedFrom(), remoteFile );

    Nepomuk2::SimpleResource website;
    website.addType( NFO::HtmlDocument() );
    website.addType( NFO::WebDataObject() );
    website.addProperty( NIE::url(), m_websiteUrl);

    QDateTime dt = QDateTime::currentDateTime();

    Nepomuk2::SimpleResource event;
    event.addType( NDO::DownloadEvent() );
    event.addProperty( NUAO::start(), dt );
    event.addProperty( NUAO::end(), dt );
    event.addProperty( NUAO::involves(), file );
    event.addProperty( NDO::referrer(), website );

    Nepomuk2::SimpleResourceGraph graph;
    graph << remoteFile << file << website << event;

    KJob* job = Nepomuk2::storeResources( graph );
    connect( job, SIGNAL(finished(KJob*)), this, SLOT(onJobFinish(KJob*)) );
}

void App::onJobFinish(KJob* job)
{
    if( job->error() ) {
        kWarning() << job->errorString();
        exit(1);
    }

    kDebug() << "Sucessfully pushed the data";
    exit(0);
}


int main( int argc, char ** argv ) {
    KAboutData aboutData("nadm",
                         "nepomuk",
                         ki18n("Nepomuk Add Download Metadata"),
                         "0.1",
                         ki18n("Nepomuk Add Download Metadata"),
                         KAboutData::License_LGPL,
                         ki18n("(c) 2012, Nepomuk-KDE Team"),
                         KLocalizedString(),
                         "http://nepomuk.kde.org");
    aboutData.addAuthor(ki18n("Vishesh Handa"), ki18n("Maintainer"), "handa.vish@gmail.com");

    KCmdLineArgs::init( argc, argv, &aboutData );

    KCmdLineOptions options;
    options.add("+fileUrl", ki18n("The downloaded file"));
    options.add("+downloadUrl", ki18n("The downloaded url"));
    KCmdLineArgs::addCmdLineOptions( options );

    KComponentData component( QByteArray("nadm") );
    App app(argc, argv);

    return app.exec();
}

#include "main.moc"
